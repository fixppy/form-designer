// layui 配置
layui.config({
	base: 'js/lay-module/',
}).extend({
	fcInputNumber:'fcInputNumber/fcInputNumber.min',
	fcDrawer:'fcDrawer/fcDrawer.min',
	cascader:'cascader/cascader',//https://fly.layui.com/extend/cascader/#doc
	fcMessage:'fcMessage/fcMessage.min'
})

layui.use(['jquery','element','form','fcInputNumber','fcDrawer','fcMessage','util','rate','slider','laydate','layedit','cascader','colorpicker','code','upload'],function(){
	var $ = layui.jquery,
		element = layui.element,
		form = layui.form,
		fcInputNumber = layui.fcInputNumber,
		fcMessage = layui.fcMessage,
		util = layui.util,
		rate = layui.rate,
		slider = layui.slider,
		laydate = layui.laydate,
		layedit = layui.layedit,
		cascader = layui.cascader,
		colorpicker = layui.colorpicker,
		fcDrawer = layui.fcDrawer,
		upload = layui.upload;

	var _el = $('#form-designer');	
	var _elSet = $('#components-setting-form');

	//表单布局总配置
	var FORMCONFIG = {
		"layout":[],
		"config":{
		    "size": "medium",			
		    "labelPosition": "right",
		    "labelWidth": 80,
		    "style":"normal",
		}
	};

	//每个组件设置保持参数
	var PARAMS = new Object();

	//初始化
	formDesignInit();
	function formDesignInit() {
    	if(sessionStorage.getItem("formdesign")){
    		var a = JSON.parse(sessionStorage.getItem("formdesign"));
    		console.log(a)
    		_el.next('.empty').hide();
    		FORMCONFIG = a.formconfig;
    		PARAMS = a.params;
    		_el.html(a.html);
    		_el.find('.active').removeClass('active');
    	}
	}

	//radio group change
	$('.radiogroup label').click(function(e){
	    if(!$(e.target).is('input')){
	        return;
	    }		
		$(this).addClass('is-active').siblings('label').removeClass('is-active');
		$(this).children('input').prop('checked',true).siblings('label').children('input').prop('checked',false);
	});

	//预览布局 drawer 配置
	fcDrawer.render({
		//clickBtn:"button[lay-active=view]",
		open:{
			elem:'#layout-view',
			title:'预览',
			width:'1200',
			// footer:false
			btn:['取消','确定'],
		},
		success:function(index){
			console.log('打开预览抽屉1')
			createView();
		},
		yes:function(index,btn){
            btnLoading.remove(btn);
            fcDrawer.close(index);
/*            setTimeout(function() {
                btnLoading.remove(btn);
                layer.msg('保存成功');
				fcDrawer.close(index);
            },2000);*/
		},
	});

	//表单top工具
	util.event('lay-active',{
		json:function() {
			//把布局信息记录在json
			jsonParams.saveLayout();
			console.log('%c setFormParams表单所有数据json：\n','background:green',FORMCONFIG)
			$('#viewpre pre').html(JSON.stringify(FORMCONFIG,null,4))
			layer.open({
				type:1,
				title:'查看json',
				area:['800px','600px'],
				skin:'alert-view-json',
				content:$('#viewpre'),
				cancel: function(index, layero){
					$('#viewpre').hide();
				}
			})
			layui.code();
		},
		view:function() {
			jsonParams.saveLayout();
			if(FORMCONFIG.layout.length < 1){
				layer.msg('请添加表单组件！')
			}else{
				console.log('预览：',FORMCONFIG)
				fcDrawer.open('#layout-view');
			}			
		},
		clear:function() {
			if(JSON.stringify(PARAMS) === '{}'){
				layer.msg('没有需要清空的组件！')
				return false;
			}
			layer.confirm('确定要清空所有组件吗？',{icon:7},function(index){
				layer.close(index);
				_el.html('');
				_el.next('.empty').show();
				FORMCONFIG.layout = '';
				PARAMS = {};
				element.tabChange('right-side-tab','2');
				_elSet.html('');
				$('.fc-f-right-side .empty').show();
				sessionStorage.removeItem("formdesign");
			})
		},
		save:function() {
			jsonParams.saveLayout();
			if(FORMCONFIG.layout.length < 1) {
				layer.msg('请添加表单组件！')
				return false;
			}else{
				fcMessage.loading('正在保存...',{
					showClose:false,
					cancel:function() {
						setTimeout(function(){
							fcMessage.close();
							layer.msg('保存成功')
						},1000)
					}
				})
				sessionStorage.setItem("formdesign",JSON.stringify({"formconfig":FORMCONFIG,"params":PARAMS,"html":_el.html()}));
			}
		},
	})

	//表单尺寸设置
	$('.radiogroup[filter="formsize"] label').click(function(e){
	    if(!$(e.target).is('input')){
	        return;
	    }
	    var val = $(this).children('input').val();
	    setFormParams({
	    	configType:'config',
	    	type:'size',
	    	data:val
	    })

	})

	//标签对齐设置
	$('.radiogroup[filter="formLabelPosition"] label').click(function(e){
	    if(!$(e.target).is('input')){
	        return;
	    }
	    var val = $(this).children('input').val();
	    setFormParams({
	    	configType:'config',
	    	type:'labelPosition',
	    	data:val
	    })
	})	

	//inputNumber 输入回调事件，自定义 -- 输入时候触发，暂时关闭，引起触发2次事件
	getLabelWidth = function(val) {
		if(val) {
		    setFormParams({
		    	configType:'config',
		    	type:'labelWidth',
		    	data:val
		    })
		}
	}

	//表单风格设置
	form.on('switch(pane)',function(data){
		var val;
		data.elem.checked?val = 'pane':val = 'normal';
	    setFormParams({
	    	configType:'config',
	    	type:'style',
	    	data:val
	    })
	})

	//表单渲染
	var formRender = {
		//初始化
		init:function(data) {
			console.log('%c 表单config：','background:blue',data.config)
			var	d = data.config;

			this.setSize(d);
			this.setLabelPosition(d);
			this.setLabelWidth(d);
			this.setType(d);
		},
		//设置表单尺寸
		setSize:function(d) {
			if(d.size == 'medium') {
				_el.removeAttr('size');
			}else{
				_el.attr("size", d.size);
			}
		},
		//设置标签对齐
		setLabelPosition:function(d) {
			if(d.labelPosition == 'right') {
				_el.removeAttr('labelPosition');
				$('[fc-filter="labelWidth"]').prop('disabled',false);
				$('[lay-filter="pane"]').prop('disabled',false);
				fcInputNumber.render();
			}else{
				_el.attr("labelPosition", d.labelPosition);
				if(d.labelPosition =='top') {
					form.val('formset',{
						"pane":0
					})
					_el.removeClass('layui-form-pane');
					_el.find('.layui-input-block').removeAttr('style');
					$('[fc-filter="labelWidth"]').prop('disabled',true);
					$('[lay-filter="pane"]').prop('disabled',true);
					FORMCONFIG["config"].type = 'normal';
				}else{
					$('[fc-filter="labelWidth"]').prop('disabled',false);
					$('[lay-filter="pane"]').prop('disabled',false);
				}
				fcInputNumber.render();
			}
			form.render(null,'formset');
		},
		//设置标签宽度
		setLabelWidth:function(d) {
			if(d.labelPosition != 'top') {
				if(d.style == 'pane') {
					_el.find('.layui-form-label').css({width:d.labelWidth+'px'});
					_el.find('.layui-input-block').css({marginLeft:(Number(d.labelWidth))+'px'});
				}else{
					_el.find('.layui-form-label').css({width:d.labelWidth+'px'});
					_el.find('.layui-input-block').css({marginLeft:(Number(d.labelWidth)+30)+'px'});
				}
			}
		},
		//设置方框风格
		setType:function(d) {
			if(d.style == 'pane' && d.labelPosition != 'top') {
				_el.addClass('layui-form-pane');
			}else{
				_el.removeClass('layui-form-pane');
			}
		},
		//初始化标签宽度
		reset:function() {
			_el.find('.layui-form-label').css({width:'80px'});
			_el.find('.layui-input-block').css({marginLeft:'110px'});
			FORMCONFIG["config"].labelWidth = 80;
			$('[fc-filter="labelWidth"]').val('80');

		},
	}

	//set form json
	function setFormParams(data) {
		if(data.configType == 'config') {
			FORMCONFIG["config"][data.type] = data.data;
			console.log(FORMCONFIG)
			formRender.init(FORMCONFIG);
		}
		if(data.configType == 'layout') {
			FORMCONFIG["layout"] = data.data;
		}
		// console.log('%c setFormParams表单所有数据json：\n','background:green',FORMCONFIG)
	}

	//表单组件list
	function components(id) {
	    var str = "";
	    /*  tool:普通组件工具结构
	     *  toolColumn:栅格布局内部容器工具结构
	     *  toolRow:栅格布局,表单布局工具结构
	     * */ 
	    var tool = '<div class="tool"><div><a href="javascript:;" lay-components="copy" title="复制"><i class="iconfont">&#xe601;</i></a><a href="javascript:;" lay-components="del" title="删除"><i class="iconfont">&#xe603;</i></a></div></div>';
	    var toolColumn = '<div class="tool hollow"><div><a href="javascript:;" lay-components="copy" title="复制" data-type="column"><i class="iconfont">&#xe601;</i></a><a href="javascript:;" lay-components="del" title="删除" data-type="column"><i class="iconfont">&#xe603;</i></a></div></div>';
	    var toolInline = '<div class="tool hollow"><div><a href="javascript:;" lay-components="copy" title="复制" data-type="inline"><i class="iconfont">&#xe601;</i></a><a href="javascript:;" lay-components="del" title="删除" data-type="inline"><i class="iconfont">&#xe603;</i></a></div></div>';
	    // var toolRow = '<div class="tool hollow"><div><a href="javascript:;" lay-components="copy" title="复制" data-type="layout"><i class="iconfont">&#xe601;</i></a><a href="javascript:;" lay-components="del" title="删除"><i class="iconfont">&#xe603;</i></a></div></div>';
	    var toolRow = '<div class="tool hollow"><div><a href="javascript:;" lay-components="del" title="删除" data-type="layout"><i class="iconfont">&#xe603;</i></a></div></div>';

        var radom = Math.ceil(Math.random() * 1000000);
	    switch (id) {
	        case "text":
	            //单行文本
	            var element = '<input type="text" class="layui-input" placeholder="请输入">';
	            str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">单行文本</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	            break;
	        case "select":
	            //下拉选择
	            var element = '<select><option>请选择</option></select>';
	            str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">下拉选择</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	            break;
	        case "textarea":
	            //多行文本
	            var element = '<textarea class="layui-textarea" placeholder="请输入"></textarea>';
	            str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">多行文本</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	            break;
	        case "radio":
	            //单选框
	            var element = '<input type="radio" name="radio_' + radom + '" checked title="选项一"><input type="radio" name="radio_' + radom + '" title="选项二">';
	            str = '<div class="layui-form-item layui-form" data-type="'+id+'" id="id_'+radom+'" lay-filter="id_'+radom+'"><label class="layui-form-label">单选</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	            break;
	        case "checkbox":
	            //多选框
	            var element = '<input type="checkbox" name="checkbox_'+radom+'" lay-skin="primary" title="选项一"><input type="checkbox" name="checkbox_'+radom+'" lay-skin="primary" title="选项二">';
	            str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">多选</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	            break;
	        case "datetime":
	            //时间
	            var element = '<input type="text" class="layui-input lay-date">';
	            str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">文本</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	            break;
	        case "InputNumber":
	        	//计数器
	        	var element = '<input class="fc-input-number" fc-filter="" value="0" />';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">计数器</label><div class="layui-input-block"><div class="v-c">' + element + '</div></div>'+tool+'</div>';
	        	break;
	       	case "switch":
	       		//开关
	       		var element = '<input type="checkbox" name="switch" lay-skin="switch">';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">开关</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "line":
	       		//分割线
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'" style="padding:5px 0;"><div class="layui-form-line"></div>'+tool+'</div>';
	        	break;
	       	case "progress":
	       		//进度条
	       		var element = '<div class="layui-progress layui-progress-big" lay-showPercent="yes" style="width:100%;"><div class="layui-progress-bar layui-bg-blue" lay-percent="50%"></div></div>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">进度条</label><div class="layui-input-block"><div class="v-c">' + element + '</div></div>'+tool+'</div>';
	       		break;
	       	case "title":
	       		//标题
	       		var element = '<h3 class="layui-form-title">标题</h3>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'">' + element + tool +'</div>';
	       		break;
	       	case "rate":
	       		//评分
	       		var element = '<div id="rate_'+radom+'" class="rate"></div>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">评分</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "link":
	       		//超链接
	       		var element = '<a href="" class="link">超链接</a>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">超链接</label><div class="layui-input-block"><div class="layui-form-mid">' + element + '</div></div>'+tool+'</div>';
	       		break;
	       	case "cascader":
	       		//级联选择器
	       		var element = '<input type="text" id="cascader_'+radom+'" class="layui-input cascader" readonly="readonly">';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">级联选择器</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "slider":
	       		//滑块
	       		var element = '<div id="slider_'+radom+'" class="slider" style="width:100%;"></div>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">滑块</label><div class="layui-input-block"><div class="v-c">' + element + '</div></div>'+tool+'</div>';
	       		break;
	       	case "dateRange":
	       		//日期范围
	       		var element = '<input type="text" class="layui-input lay-date" placeholder="">';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">日期范围</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "date":
	       		//日期
	       		var element = '<input type="text" class="layui-input lay-date" placeholder="">';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">日期</label><div class="layui-input-inline">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "time":
	       		//时间
	       		var element = '<input type="text" class="layui-input lay-time" placeholder="">';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">时间</label><div class="layui-input-inline">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "editor":
	       		//编辑器
	       		var element = '<textarea id="editor'+radom+'" class="editor" style="display: none;"></textarea>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">编辑器</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "uploadImg":
	       		//图片上传
	       		var element = '<div class="upload-img single" id="uploadimg_'+radom+'"><div class="action"><i class="layui-icon layui-icon-upload-drag"></i><p>上传</p></div></div>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">图片上传</label><div class="layui-input-block">' + element + '</div>'+tool+'</div>';
	       		break;
	       	case "uploadFile":
	       		//附件
	       		var element = '<button type="button" class="layui-btn layui-btn-sm layui-btn-normal" id="uploadfile_'+radom+'"><i class="layui-icon">&#xe67c;</i>上传文件</button>';
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'"><label class="layui-form-label">附件</label><div class="layui-input-block"><div class="v-c">' + element + '</div></div>'+tool+'</div>';
	       		break;
	       	case "gridLayout":
	       		//栅格布局
	        	str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'">'+
        			  '		<div class="layui-row layui-col-space20">'+
        			  '			<div class="layui-col-md6 column">'+
	        		  '				<div class="layout droppable"></div>'+
	        		  '				'+toolColumn+''+
        			  			'</div>'+
        			  '			<div class="layui-col-md6 column">'+
	        		  '				<div class="layout droppable"></div>'+
	        		  '				'+toolColumn+''+
        			  '			</div>'+
        			  '		</div>'+
        			  '		'+toolRow+''+
        			  '</div>';
	       		break;
	       	case "formLayout":
	       		//表单布局
	       		str = '<div class="layui-form-item" data-type="'+id+'" id="id_'+radom+'">'+
	       			  '		<div class="layui-inline droppable layui-inline-col">'+toolInline+'</div>'+
	       			  '		<div class="layui-inline droppable layui-inline-col">'+toolInline+'</div>'+
	       			  '		<div class="layui-inline droppable layui-inline-col">'+toolInline+'</div>'+
	       			  '		'+toolRow+''+
	       			  '</div>';
	       		break;
	    }
	    return str;
	}

	//点击添加组件
	$('.fc-components-list li').click(function(){
		var type = $(this).data('type');
		var isActiveComponent = _el.find('.layui-form-item.active');
		var isActiveLayout = _el.find('[data-type="gridLayout"] .layui-form-item.active');
		var isActiveColumn = _el.find('[data-type="gridLayout"] .column.active');
		var isActiveInline = _el.find('.layui-inline-col.active');

		//隐藏空结构
		_el.next('.empty').hide();

		if(isActiveComponent.length > 0) {
			//当前位置是组件
			if(isActiveComponent.parent().is('.layui-inline-col')){
				layer.msg('内联容器中只能添加一个组件');
				return false;
			}
			if(type == 'gridLayout' && isActiveComponent.parent().is('.layout')) {
				//如果新添加组件是栅格组件并且当前位置组件在栅格容器内
				_el.find('.active').removeClass('active');
				$(components(type)).insertAfter(isActiveComponent.parents('.layui-form-item')).addClass('active');
			}else if(type == 'formLayout' && isActiveComponent.parent().is('.layout')){
				//新添加组件是表单布局，并且当前位置组件在栅格容器内
				layer.msg('栅格布局内不能添加表单布局');
				return false;
			}else{
				//表单框中已存在组件，在该元素之后插入新组件
				$(components(type)).insertAfter(isActiveComponent).addClass('active').siblings().removeClass('active');
			}
		}else if(isActiveLayout.length > 0) {
			//当前位置是栅格布局内组件，在该组件后插入新组件
			_el.find('.active').removeClass('active');
			$(components(type)).appendTo(isActiveLayout.children('.layout')).addClass('active').siblings().removeClass('active');
			isActiveLayout.attr('full','');
		}else if(isActiveColumn.length > 0){
			//当前位置是栅格布局column
			if(type == 'gridLayout') {
				//添加栅格组件
				_el.find('.active').removeClass('active');
				$(components(type)).insertAfter(isActiveColumn.parents('.layui-form-item')).addClass('active');
			}else if(type == 'formLayout') {
				//添加表单布局
				layer.msg('栅格布局内不能添加表单布局');
				return false;				
			}else{
				//普通组件
				_el.find('.active').removeClass('active');
				isActiveColumn.attr('full','');
				$(components(type)).appendTo(isActiveColumn.children('.layout')).addClass('active').siblings().removeClass('active');
			}
		}else if(isActiveInline.length > 0) {
			//当前位置layui-inline 内联结构
			if(type == 'gridLayout' || type == 'formLayout') {
				//添加栅格组件,表单布局组件
				_el.find('.active').removeClass('active');
				$(components(type)).insertAfter(isActiveInline.parents('.layui-form-item')).addClass('active');
			}else{
				//普通组件
				_el.find('.active').removeClass('active');
				isActiveInline.attr('full','').removeClass('droppable');
				$(components(type)).prependTo(isActiveInline).addClass('active').siblings().removeClass('active');
			}
		}else{
			//表单框是空的
			$(components(type)).appendTo(_el).addClass('active').siblings().removeClass('active');
		}

		//获取当前添加组件的id
		var id = _el.find('[data-type="'+type+'"].active').attr('id');
		//新添加组件
		addComponents(type,id);

		console.log('%c 点击添加组件的类型与ID:','background:LimeGreen;color:#000',type,id)
	})

	//拖拽添加组件
	var el_draggable = function() {
		$( ".draggable" ).draggable({
		    connectToSortable: ".droppable",
		    helper: "clone",
		    zIndex:100,
		    // stop:function(event,ui) {
		    // 	console.log(event,ui)
		    // }
		}).disableSelection();

		$( ".droppable" ).sortable({
		    // accept: '.draggable',
		    //connectWith: '.droppable',
			start: function(e, ui ){
			     ui.placeholder.height(ui.helper.outerHeight());
			},		    
		    receive: function (event, ui) {  
		    	console.log(ui)   
		        var $orig = $(ui.helper);
		        var type = $orig.data('type');

		    	// console.log('%c 拖拽结束并放置组件：','color:LimeGreen',type)
		    	// console.log('拖拽目标位置：',this)
		    	console.log('拖拽组件：',type)

				//隐藏空结构
				$(this).next('.empty').hide();

				if(type == 'gridLayout') {
		        	if($(this).is('.layout')) {
						//拖拽的新栅格组件不能拖拽进已有的栅格组件中
						$(this).parents('.layui-form').find('.active').removeClass('active');
						$(this).find('li[data-type="'+type+'"]').remove();
						$(components(type)).insertAfter($(this).parents('.layui-form-item')).addClass('active');
		        	}else if($(this).is('.layui-inline-col')) {
						$(this).find('li[data-type="'+type+'"]').remove();
						layer.msg('表单布局内不能添加栅格布局');
		        	}else{
						//拖拽的新栅格组件不能拖拽进已有的栅格组件中
						$(this).find('.active').removeClass('active');
						$(this).find('li[data-type="'+type+'"]').replaceWith($(components(type)).addClass('active'));
		        	}
				}else if(type == 'formLayout') {
					if($(this).is('.layout')) {
						$(this).find('li[data-type="'+type+'"]').remove();
						layer.msg('栅格布局内不能添加表单布局');
					}else if($(this).is('.layui-inline-col')) {
						$(this).find('li[data-type="'+type+'"]').remove();
						layer.msg('表单布局内不能添加表单布局');						
					}else{
						console.log('sss')
						$(this).find('li[data-type="'+type+'"]').replaceWith($(components(type)).addClass('active'));
					}
				}else{
			        if($(this).is('.layout')) {
			        	//拖拽位置在栅格布局的layou容器中
			        	$(this).parent('.column').attr('full','');
			        	$(this).parents('.layui-form').find('.active').removeClass('active');
			        }else if($(this).is('.layui-inline-col')) {
			        	$(this).attr('full','');
			        	$(this).removeClass('active droppable');
			        	$(this).parents('.layui-form').find('.active').removeClass('active');
			        }else{
			        	$(this).find('.active').removeClass('active');
			        }
			        //用新组件代替拖拽中的元素
			        $(this).find('li[data-type="'+type+'"]').replaceWith($(components(type)).addClass('active'));
				}

				// console.log(ui.sender[0])
				// 栅格布局中移除组件时候，列中没有组件则恢复原样
				if($(ui.sender[0]).is('.layout')) {
					var len = $(ui.sender[0]).children('.layui-form-item').length;
					if(len == 0) {
						$(ui.sender[0]).parent().removeAttr('full');
					}
				}

				//表单布局中移除组件，原位置恢复原样
				if($(ui.sender[0]).is('.layui-inline-col')) {
					$(ui.sender[0]).removeAttr('full');
				}

				//获取当前添加组件的id
				var id = _el.find('[data-type="'+type+'"].active').attr('id');
				// //新添加组件
				addComponents(type,id);
				console.log('%c 拖拽添加组件的类型与ID:','background:LimeGreen;color:#000',type,id)
		    },
		    over:function(event,ui) {
		    	//console.log('拖拽进入容器over')
		    },
		    stop:function(event,ui) {
 				//记录sort后的id顺序数组
        		// var arr = $( ".droppable" ).sortable('toArray');
          //   	console.log('主结构排序',arr);		
		    }
		}).disableSelection();
	};
	el_draggable();

	//新添加组件
	function addComponents(type,id) {
		//把新增组件中的部分信息存入配置参数
		jsonParams.saveParams(type);

		//显示组件对应设置
		addComponentsSetting(type);

		//渲染组件
		form.render();
		element.init();
		fcInputNumber.render();
		var _params = Object.assign({}, PARAMS[id]);
		_params.id = $('#'+id).find('[id]').attr('id');
		componentsRender(_params);
	}

	//components tool event
	util.event('lay-components',{
		copy:function() {
			var type = $(this).data('type'),
				id = $(this).parents('.active').attr('id');
				
			console.log('复制组件:',type,id)
			var radom = Math.ceil(Math.random() * 1000);
			//备注：栅格布局与内联布局去掉了copy功能，column/inline/layout
			if(type == 'column') {
				//栅格布局
				var that = $(this).parents('.column.active');
				var clone = that.clone();
				that.removeClass('active');
				clone.removeAttr('full');
				clone.find('.layout').children('.layui-form-item').remove();
				that.after(clone);
				el_draggable();
			}else if(type == 'inline') {
				//内联布局
				var that = $(this).parents('.layui-inline-col.active');
				var clone = that.clone();
				that.removeClass('active');
				that.after(clone);
				el_draggable();
			}else if(type == 'layout') {
				// 栅格布局
				var that = $(this).parents('.layui-form-item.active');
				var clone = that.clone();
				that.removeClass('active');
				that.after(clone);

				//copy组件 修改当前选中的新id
				var _copyComponent = _el.find('.layui-form-item.active'),
					_oldId = _copyComponent.attr('id'),
					_newId = _oldId+'_copy_'+radom;
				_copyComponent.attr('id',_newId);
				var obj2 = Object.assign({}, PARAMS[_oldId]);
				PARAMS[_newId] = Object.assign({},obj2);
				//如果栅格布局内还有组件，找出含有id的组件，添加新id
				if(_el.find('.layui-form-item.active').find('[id]').length > 0) {
					_el.find('.layui-form-item.active').find('[id]').map(function(i,d){
						var _oldId = $(d).attr('id');
						var _newId = _oldId+'_copy_'+radom;
						$(d).attr('id',_newId);
						var obj2 = Object.assign({}, PARAMS[_oldId]);
						PARAMS[_newId] = Object.assign({},obj2);
					})
				}
				el_draggable();
			}else {
				//普通组件
				if($(this).parents('.layui-inline-col').length > 0){
					layer.msg('内联布局中不能复制组件');
					return false;
				}
				var that = $(this).parents('.layui-form-item.active');
				var clone = that.clone();
				that.removeClass('active');
				that.after(clone);

				//copy组件 修改当前选中的新id
				var _copyComponent = _el.find('.layui-form-item.active'),
					_oldId = _copyComponent.attr('id'),
					_newId = _oldId+'_copy_'+radom;
				_copyComponent.attr('id',_newId);
				_copyComponent.attr('lay-filter',_newId);

				var _n = _copyComponent.find('[type="radio"]').attr('name');
				if(_n) {
					_copyComponent.find('[type="radio"]').attr('name',_n+'_copy_'+radom);
				}

				var obj2 = Object.assign({}, PARAMS[_oldId]);
				var newObject = jQuery.extend(true, {}, obj2);
				PARAMS[_newId] = newObject;

				//查找组件内是否含有其他id元素,存入新id对象中
				if(_copyComponent.find('[id]').length > 0){
					var _x = _copyComponent.find('[id]').attr('id');
					_copyComponent.find('[id]').attr('id',_x+'_copy_'+radom);
					PARAMS[_newId].id = _x+'_c_'+radom
				}
			}
			console.log(PARAMS)
		},
		del:function() {
			var type = $(this).data('type'),
			id = $(this).parents('.active').attr('id');
			console.log('%c删除组件:','color:red',type,id)

			//判断是删除布局 或 组件，column:栅格布局
			if(type == 'column') {
				//栅格布局删除
				var that = $(this).parents('.column');
				//内部有组件，同步删除组件id对应的PARAMS中数据
				if(that.next().length > 0) {
					that.next().addClass('active');
					that.remove();
				}else if(that.prev().length > 0){
					that.prev().addClass('active');
					that.remove();
				}else if(that.parents('.layui-form-item').next().length > 0){
					that.parents('.layui-form-item').next().addClass('active');
					that.parents('.layui-form-item').remove();
				}else{
					that.parents('.layui-form-item').prev().addClass('active');
					that.parents('.layui-form-item').remove();
				}
			}else if(type == 'inline') {
				//内联布局删除
				var that = $(this).parents('.layui-inline-col');
				if(that.parent().children('.layui-inline-col').length > 1) {
					that.remove();
				}else{
					that.parent().remove();
				}
			}else if(type == 'layout') {
				//布局组件整个删除，需遍历内部是否存在其他组件
				var that = $(this).parents('.layui-form-item.active');
				var findComponent = that.find('.layui-form-item[data-type]');
				var cLen = findComponent.length;
				//内部有组件，同步删除组件id对应的PARAMS中数据
				if(cLen > 0) {
					findComponent.map(function(i,d) {
						var cId = $(d).attr('id');
						delete(PARAMS[cId]);	
					})
				}
				that.remove();
			}else{
				//普通组件删除
				var that = $(this).parents('.layui-form-item.active');

				if(that.parent().is('.layui-inline-col')){
					//内联布局中只允许一个组件，删除组件时候就恢复内联初始状态
					that.parent().removeAttr('full').addClass('droppable');
				}else if(that.next().length > 0) {
					//下级还存在组件
					that.next().addClass('active');
				}else if(that.prev().length > 0) {
					//上级还存在组件
					that.prev().addClass('active');
				}else{
					//栅格组件中移除组件
					that.parents('.column').removeAttr('full').addClass('active');
				}
				that.remove();
			}

			delete(PARAMS[id]);
			console.log(PARAMS)

			//表单中组件都删除了，显示空结构			
			if(_el.children().length === 0) {
				_el.next('.empty').show();
			}
		}
	})

	//components click event && display the settings
	$(document).on('click','#form-designer .layui-form-item',function(e) {
		// console.log(e.target)
		var type = $(this).data('type');
		var id = $(this).attr('id');
		// console.log('%c ---当前点击组件---','background:SlateBlue',this)
		console.log('%c 当前点击组件名称：'+type+',id:'+id+'','background:SlateBlue')

		//如果点击的是组件操作按钮icon，停止切换
		if($(e.target).is('i')) {
			return false;
		}

		addComponentsSetting(type,id);

		//栅格布局中点击切换
		if($(e.target).is('.column')) {
			$(this).parents('.layui-form').find('div.active').removeClass('active');
			$(e.target).addClass('active');
			return false;
		}

		//栅格布局中点击组件
		if($(e.target).is('.tool')) {
			$(this).parents('.layui-form').find('div.active').removeClass('active');
			$(e.target).parent().addClass('active');
			return false;
		}

		//栅格布局中layout容器
		if($(e.target).is('.layout')) {
			$(this).parents('.layui-form').find('.active').removeClass('active');
			$(e.target).parent().addClass('active');
			return false;
		}

		//内联布局
		if($(e.target).is('.layui-inline-col')) {
			$(this).parents('.layui-form').find('.active').removeClass('active');
			$(e.target).addClass('active');
			return false;
		}

		//选择当前组件
		$(this).parents('.layui-form').find('.active').removeClass('active');
		$(this).addClass('active').siblings().removeClass('active');

	})

	//表单组件对应配置结构列表
	function settingList(id) {
	    var str = "";
	    switch (id) {
	        case "text":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="单行文本" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">占位提示</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="请输入" name="placeholder" lay-settings="placeholder"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">组件宽度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setInputWidth" value="100" name="width" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">%</div>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "textarea":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="多行文本" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">占位提示</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="请输入" name="placeholder" lay-settings="placeholder"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">组件宽度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setInputWidth" value="100" name="width" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">%</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">组件高度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setTextareaHeight" value="100" max="500" min="100" step="5" name="height" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">px</div>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "radio":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="单选" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">选项</label>'+
	        		  '		<div class="layui-input-block settings-change-input radio-group" type="radio">'+
	        		  '			<div class="item">'+
	        		  '				<div><input type="radio" name="radio" value="" title="" checked lay-filter="set-radio-select"><input class="layui-input" value="选项一"></div>'+
	        		  '				<div><input type="radio" name="radio" value="" title="" lay-filter="set-radio-select"><input class="layui-input" value="选项二"></div>'+
	        		  '			</div>'+
	        		  '			<a href="javascript:;" lay-set-radio="add"><i class="layui-icon layui-icon-add-circle"></i> 添加选项</a>'+
	        		  '		</div>'+
	        		  '</div>'
	        	break;
	        case "checkbox":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="多选" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">选项</label>'+
	        		  '		<div class="layui-input-block settings-change-input checkbox-group" type="checkbox">'+
	        		  '			<div class="item">'+
	        		  '				<div><input type="checkbox" name="checkbox" value="" title="" lay-skin="primary" lay-filter="set-checkbox-select"><input class="layui-input" value="选项一"></div>'+
	        		  '				<div><input type="checkbox" name="checkbox" value="" title="" lay-skin="primary" lay-filter="set-checkbox-select"><input class="layui-input" value="选项二"></div>'+
	        		  '			</div>'+
	        		  '			<a href="javascript:;" lay-set-checkbox="add"><i class="layui-icon layui-icon-add-circle"></i> 添加选项</a>'+
	        		  '		</div>'+
	        		  '</div>'
	        	break;
	        case "select":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="下拉选择" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">选项</label>'+
	        		  '		<div class="layui-input-block settings-change-input radio-group" type="select">'+
	        		  '			<div class="item">'+
	        		  '				<div><input type="radio" name="radio" value="0" title="" checked lay-filter="set-select-option"><input class="layui-input" value="请选择"></div>'+
	        		  '			</div>'+
	        		  '			<a href="javascript:;" lay-set-select="add"><i class="layui-icon layui-icon-add-circle"></i> 添加选项</a>'+
	        		  '		</div>'+
	        		  '</div>'
	        	break;
	        case "InputNumber":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="计数器" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">初始值</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="0" name="value" lay-settings="inputnumber"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">最小值</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="" name="min" lay-settings="inputnumber" placeholder="默认值：0"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">最大值</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="" name="max" lay-settings="inputnumber" placeholder="默认值：100"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">自定义步数</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="" name="step" lay-settings="inputnumber" placeholder="默认值：1"></div>'+
	        		  '</div>'
	        	break;
	        case "switch":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="开关" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">是否开启</label>'+
	        		  '		<div class="layui-input-block"><input type="checkbox" name="switch" lay-skin="switch" lay-filter="set-switch"></div>'+
	        		  '</div>'
	        	break;
	        case "line":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">上间距</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setLineMarginTop" value="" name="marginTop" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">px</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">下间距</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setLineMarginBottom" value="" name="marginBottom" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">px</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">线条</label>'+
	        		  '		<div class="layui-input-block"><input type="radio" name="radio_line" value="solid" title="实线" checked lay-filter="set-line-border"><input type="radio" name="radio_line" value="dashed" title="虚线" lay-filter="set-line-border"></div>'+
	        		  '</div>'
	        	break;
	        case "progress":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="进度条" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">进度条尺寸</label>'+
	        		  '		<div class="layui-input-block"><input type="radio" name="progress_size" value="big" title="大" checked lay-filter="set-progress-size"><input type="radio" name="progress_size" value="mini" title="小" lay-filter="set-progress-size"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">文本显示</label>'+
	        		  '		<div class="layui-input-block"><input type="checkbox" name="progress_txt" checked lay-skin="switch" lay-filter="setProgressTxt"></div>'+
	        		  '</div>'
	        	break;
	        case "title":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="标题" name="title" lay-settings="titleFont"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">字号</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setTitleFontSize" value="18" min="12" name="fontSize" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">px</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">粗体</label>'+
	        		  '		<div class="layui-input-block"><input type="checkbox" name="fontWeight" lay-skin="switch" lay-filter="setTitleFontWeight"></div>'+
	        		  '</div>'	        		  
	        	break;
	        case "rate":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="评分" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">初始值</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setRateValue" value="0" max="5" name="value" />'+
					  '			</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">颜色</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div id="setRateColor"></div>'+
					  '		</div>'+
	        		  '</div>'+		        		  
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">只读</label>'+
	        		  '		<div class="layui-input-block"><input type="checkbox" name="readonly" lay-skin="switch" lay-filter="setRateReadonly"></div>'+
	        		  '</div>'
	        	break;
	        case "link":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="超链接" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">链接文字</label>'+
					  '		<div class="layui-input-block">'+
					  '			<input class="layui-input" value="超链接" name="text" lay-settings="setLinkText">'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">链接地址</label>'+
					  '		<div class="layui-input-block">'+
					  '			<input class="layui-input" value="" name="href" lay-settings="setLinkHref">'+
					  '		</div>'+
	        		  '</div>'	        		  
	        	break;
	        case "cascader":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="级联选择器" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">数据源</label>'+
					  '		<div class="layui-input-block">'+
					  '	      <select name="source" lay-filter="cascader-source">'+
					  '	        <option value=""></option>'+
					  '	        <option value="0">北京</option>'+
					  '	        <option value="1">上海</option>'+
					  '	      </select>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "slider":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="滑块" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">开启输入框</label>'+
	        		  '		<div class="layui-input-block"><input type="checkbox" name="input" lay-skin="switch" lay-filter="setSliderInput"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">初始值</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setSliderValue" value="0" name="value" />'+
					  '			</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">最小值</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setSliderMin" value="0" name="min" />'+
					  '			</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">最大值</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setSliderMax" value="100" name="max" />'+
					  '			</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">步长</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setSliderStep" value="0" name="step" />'+
					  '			</div>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "dateRange":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="日期范围" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">占位提示</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="" name="placeholder" lay-settings="placeholder"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">组件宽度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setInputWidth" value="100" name="width" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">%</div>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "date":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="日期" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">占位提示</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="" name="placeholder" lay-settings="placeholder"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">组件宽度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setInputWidth" value="100" name="width" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">%</div>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "time":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="时间" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">占位提示</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="" name="placeholder" lay-settings="placeholder"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">组件宽度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setInputWidth" value="100" name="width" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">%</div>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "editor":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="编辑器" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">组件高度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setEditorHeight" value="180" max="1000" step="10" name="height" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">px</div>'+
					  '		</div>'+
	        		  '</div>'
	        	break;
	        case "uploadImg":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="图片上传" name="label" lay-settings="title"></div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">上传数量</label>'+
	        		  '		<div class="layui-input-block"><input type="radio" name="uploadimg" value="single" title="单张" checked lay-filter="set-uploadimg-number"><input type="radio" name="uploadimg" value="multiple" title="多张" lay-filter="set-uploadimg-number"></div>'+
	        		  '</div>'
	        	break;
	        case "uploadFile":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">标题</label>'+
	        		  '		<div class="layui-input-block"><input class="layui-input" value="附件" name="label" lay-settings="title"></div>'+
	        		  '</div>'
	        	break;
	        case "gridLayout":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">栅格间隔</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setGridLayoutSpace" value="20" max="30" step="2" name="space" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">px</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">水平排列</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setGridLayoutCol" value="2" min="1" max="4" name="col" />'+
					  '			</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div cLenlayui-form-item>'+
	        		  '		<div class="layui-input-block">'+
	        		  '			<a href="javascript:;" lay-set-gridLayout="add"><i class="layui-icon layui-icon-add-circle"></i> 添加列</a>'+
	        		  '		</div>'+
	        		  '</div>'	        		  
	        	break;
	        case "formLayout":
	        	str = '<div class="layui-form-item">'+
	        		  '		<label class="layui-form-label">列宽度</label>'+
					  '		<div class="layui-input-block">'+
					  '			<div class="layui-input-inline" style="width:132px;">'+
					  '				<input class="fc-input-number" change="setFormLayoutWidth" value="310" min="310" max="1000" step="10" name="colWidth" />'+
					  '			</div>'+
					  '			<div class="layui-form-mid">px</div>'+
					  '		</div>'+
	        		  '</div>'+
	        		  '<div cLenlayui-form-item>'+
	        		  '		<div class="layui-input-block">'+
	        		  '			<a href="javascript:;" lay-set-formlayout="add"><i class="layui-icon layui-icon-add-circle"></i> 添加列</a>'+
	        		  '		</div>'+
	        		  '</div>'
	        	break;
        }
        return str;
	}

	//表单组件对应配置结构写入
	function addComponentsSetting(type,id) {
		element.tabChange('right-side-tab','1');
		$('.fc-f-right-side .empty').hide();
		
		//填入目标,右侧组件属性结构
        _elSet.html(settingList(type));

		//获取组件配置信息
		if(id) {
			jsonParams.getParams(type,id);
		}
		
		form.render(null,'components-setting-form');
		element.init();
		fcInputNumber.render();

		if(type == 'rate') {
			changeRateTheme(type,id);
		}

	}

	//json写入与读取，布局与参数分开保存
	var jsonParams = {
		//每次对组件布局的调整，都遍历结构，把结构排序保存在json中
		saveLayout:function() {
			var jsonData = [];

			var components = _el.children('.layui-form-item[data-type]');
			components.map(function(i,d) {
				var _type = $(d).data('type'),
					_id = $(d).attr('id');

				if(_type == 'gridLayout' || _type == 'formLayout') {
					//栅格布局，查找内部是否存在组件，空就不保存。有组件就记录列数，并对应存入
					var _childrenArr = new Array();
					var _children = $(d).find('.layui-form-item[data-type]').length;
					if(_children > 0) {
						var _column;
						if(_type == 'gridLayout'){
							_column = $(d).find('.column');
						}else if(_type == 'formLayout') {
							_column = $(d).find('.layui-inline');
						}
						//列数量
						for(i=0;i<_column.length;i++) {
							var _o = [];
							var _a = _column.eq(i).find('.layui-form-item[data-type]');
							var _b = _a.attr('id');
							_a.map(function(index,item) {
								var _aid = $(item).attr('id');
								_o.push(PARAMS[_aid]);
							})
							_childrenArr.push(_o);
						}
						jsonData.push(PARAMS[_id]);
						PARAMS[_id].children = _childrenArr
					}
				}else{
					jsonData.push(PARAMS[_id]);
				}

			})

			console.log('%c 布局JSON','background:red;color:#fff',jsonData)

			//把布局信息记录在json
			setFormParams({
				configType:'layout',
				data:jsonData,
			});
		},
		//保存对应组件的设置参数，参数是独立保存的
		saveParams:function(type) {
			switch (type) {
				case 'text':
				case 'textarea':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html(),
						placeholder = o.find('[placeholder]').attr('placeholder');
					PARAMS[id] = {
						"name":type,
						"label":label,
						"placeholder":placeholder,
					}
					break;
				case 'radio':
				case 'checkbox':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html(),
						array = [];
					o.find('input').map(function(i,d){
						var title = $(d).attr('title')
						var checked = $(d).prop('checked');
						array.push({"title":title,"checked":checked})
					})
					PARAMS[id] = {
						"name":type,
						"label":label,
						"option":array
					}
					break;
				case 'select':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html(),
						array = [];
					o.find('select>option').map(function(i,d){
						var name = $(d).html();
						var selected = $(d).prop('selected');
						array.push({"name":name,"checked":selected})
					})
					PARAMS[id] = {
						"name":type,
						"label":label,
						"option":array
					}
					break;
				case 'InputNumber':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html(),
						array = {min:'',max:'',value:'0',step:''};
					PARAMS[id] = {
						"name":type,
						"label":label,
						"config":array
					}
					break;
				case 'switch':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html(),
						checked = o.find('input').prop('checked');
					PARAMS[id] = {
						"name":type,
						"label":label,
						"checked":checked
					}
					break;
				case 'line':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id');
					PARAMS[id] = {
						"name":type,
						"borderStyle":'',
						"marginTop":'',
						"marginBottom":''
					}
					break;
				case 'progress':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"showPercent":true,
						"size":'big',
					}
					break;
				case 'title':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						title = o.find('h3').html();
					PARAMS[id] = {
						"name":type,
						"title":title,
						"fontSize":'18',
						'fontWeight':false
					}
					break;
				case 'rate':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"value":0,
						"readonly":false,
						"theme":'#FFB800',
						"id":'rate_'+id
					}
					break;
				case 'link':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html(),
						text = o.find('a').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"href":'',
						"text":text
					}
					break;
				case 'cascader':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'cascader_'+id,
						"data":''
					}
					break;
				case 'slider':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'slider_'+id,
						"value":0,
						"input": false,
						"min":0,
						"max":100,
						"step":1,
						"theme":'#1E9FFF',
					}
					break;
				case 'dateRange':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'dateRange_'+id,
						"value":'',
						"type":'date',
						"range": true,
						"placeholder":'',
					}
					break;
				case 'date':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'date_'+id,
						"value":'',
						"type":'date',
						"placeholder":'',
					}
					break;
				case 'time':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'time'+id,
						"value":'',
						"type":'time',
						"placeholder":'',
					}
					break;
				case 'editor':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'editor_'+id,
					}
					break;
				case 'uploadImg':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'uploadimg_'+id,
						"multiple":false
					}
					break;
				case 'uploadFile':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						label = o.find('label').html();
					PARAMS[id] = {
						"name":type,
						"label":label,
						"id":'uploadFile_'+id,
					}
					break;
				case 'gridLayout':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id'),
						col = o.find('.column').length;
					PARAMS[id] = {
						"name":type,
						"col":col,
						"space":20,
						"children":[]
					}
					break;
				case 'formLayout':
					var o = _el.find('[data-type="'+type+'"].active');
					var id = o.attr('id');
					PARAMS[id] = {
						"name":type,
						"colWidth":"310",
						"children":[]
					}
					break;
			}
			console.log('%c 保存对应组件的初始设置参数:','color:gold',PARAMS)
		},
		//获取对应组件的设置参数,并赋值
		getParams:function(type,id) {
			console.log(PARAMS)
			// console.log(type,id)
			console.log('获取对应组件的设置参数',PARAMS[id]);
			if(type == 'text' || type == 'textarea') {
				for(key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
			}

			if(type == 'radio') {
				_elSet.find('[name="label"]').val(PARAMS[id].label);
				_elSet.find('.settings-change-input[type="radio"]>div').html('');
				PARAMS[id].option.map(function(d,i){
					// console.log(d,i)
					if(i < 2) {
						_elSet.find('.settings-change-input[type="radio"]>.item').append('<div><input type="radio" name="radio" value="" title="" lay-filter="set-radio-select" '+(d.checked?"checked":"")+'><input class="layui-input" value="'+d.title+'"></div>');
					}else{
						_elSet.find('.settings-change-input[type="radio"]>.item').append('<div><input type="radio" name="radio" value="" title="" lay-filter="set-radio-select" '+(d.checked?"checked":"")+'><input class="layui-input" value="'+d.title+'"><a href="javascript:;" lay-set-radio="del"><i class="layui-icon layui-icon-reduce-circle"></i></a></div>');
					}
					_el.find('#'+id).find('input').eq(i).prop('checked',d.checked);
				})
			}

			if(type == 'checkbox') {
				_elSet.find('[name="label"]').val(PARAMS[id].label);
				_elSet.find('.settings-change-input[type="checkbox"]>div').html('');
				PARAMS[id].option.map(function(d,i){
					// console.log(d,i)
					if(i < 2) {
						_elSet.find('.settings-change-input[type="checkbox"]>.item').append('<div><input type="checkbox" name="checkbox" value="" lay-skin="primary" title="" lay-filter="set-checkbox-select" '+(d.checked?"checked":"")+'><input class="layui-input" value="'+d.title+'"></div>');
					}else{
						_elSet.find('.settings-change-input[type="checkbox"]>.item').append('<div><input type="checkbox" name="checkbox" value="" title="" lay-skin="primary"  lay-filter="set-checkbox-select" '+(d.checked?"checked":"")+'><input class="layui-input" value="'+d.title+'"><a href="javascript:;" lay-set-checkbox="del"><i class="layui-icon layui-icon-reduce-circle"></i></a></div>');
					}
					_el.find('#'+id).find('input').eq(i).prop('checked',d.checked);
				})
			}

			if(type == 'select') {
				_elSet.find('[name="label"]').val(PARAMS[id].label);
				_elSet.find('.settings-change-input[type="select"]>div').html('');
				PARAMS[id].option.map(function(d,i){
					// console.log(d,i)
					if(i < 1) {
						_elSet.find('.settings-change-input[type="select"]>.item').append('<div><input type="radio" name="radio" value="" lay-skin="primary" title="" lay-filter="set-select-option" '+(d.checked?"checked":"")+'><input class="layui-input" value="'+d.name+'"></div>');
					}else{
						_elSet.find('.settings-change-input[type="select"]>.item').append('<div><input type="radio" name="radio" value="" title="" lay-skin="primary"  lay-filter="set-select-option" '+(d.checked?"checked":"")+'><input class="layui-input" value="'+d.name+'"><a href="javascript:;" lay-set-select="del"><i class="layui-icon layui-icon-reduce-circle"></i></a></div>');
					}
				})
			}

			if(type == 'InputNumber') {
				_elSet.find('[name="label"]').val(PARAMS[id].label);
				for(var key in PARAMS[id].config) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id].config[key]);
				}
				_el.find('#'+id).find('input').val(PARAMS[id].config.value);
			}

			if(type == 'switch') {
				_elSet.find('[name="label"]').val(PARAMS[id].label);
				form.val('components-setting-form',{
					'switch':PARAMS[id].checked
				})
			}

			if(type == 'line') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
				_elSet.find('input[name="radio_line"][value="'+ PARAMS[id].borderStyle+'"]').prop('checked','checked');
			}

			if(type == 'progress') {
				_elSet.find('[name="label"]').val(PARAMS[id].label);
				_elSet.find('input[name="progress_size"][value="'+ PARAMS[id].size+'"]').prop('checked','checked');
				form.val('components-setting-form',{
					'progress_txt':PARAMS[id].showPercent
				})		
			}

			if(type == 'title') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
				form.val('components-setting-form',{
					'fontWeight':PARAMS[id].fontWeight
				})
			}

			if(type == 'rate') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
				form.val('components-setting-form',{
					'readonly':PARAMS[id].readonly
				})
			}

			if(type == 'link') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
			}

			if(type == 'cascader') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}				
				_elSet.find('select[name="source"] option[value='+PARAMS[id].data+']').prop('selected',true);
			}

			if(type == 'slider') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
				form.val('components-setting-form',{
					'input':PARAMS[id].input
				})
			}

			if(type == 'dateRange' || type == 'date' || type == 'time') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
			}

			if(type == 'editor') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
			}

			if(type == 'uploadImg') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
				_elSet.find('input[name="uploadimg"][value="'+ (PARAMS[id].multiple?"multiple":"single")+'"]').prop('checked','checked');
			}

			if(type == 'uploadFile') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
			}

			if(type == 'gridLayout') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
			}

			if(type == 'formLayout') {
				for(var key in PARAMS[id]) {
					_elSet.find('[name="'+key+'"]').val(PARAMS[id][key]);
				}
			}
		}
	}

	/**
	 * 组件属性中一些针对组件的操作
	 */
	//input组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//修改编辑标题
	$(document).on('input propertychange','input[lay-settings="title"]',function() {
		$(this).on('input propertychange', function() {
			var id = _el.find('.layui-form-item.active').attr('id');
			var val = $(this).val();
			_el.find('.layui-form-item.active').find('label').html(val);
			PARAMS[id].label = val;
		})
	});

	//修改占位提示
	$(document).on('input propertychange','input[lay-settings="placeholder"]',function() {
		$(this).on('input propertychange', function() {
			var id = _el.find('.layui-form-item.active').attr('id');
			var val = $(this).val();
			_el.find('.layui-form-item.active').find('[placeholder]').attr('placeholder',val);
			PARAMS[id].placeholder = val;
		})
	});

	//修改input宽度
	setInputWidth = function(val) {
		var id = _el.find('.layui-form-item.active').attr('id');
		_el.find('.layui-form-item.active').find('input,textarea').css({width:val+'%'});
		PARAMS[id].width = val;
	}	

	//修改 textarea 高度
	setTextareaHeight = function(val) {
		var id = _el.find('.layui-form-item.active').attr('id');
		_el.find('.layui-form-item.active').find('textarea').css({height:val+'px'});
		PARAMS[id].height = val;		
	}

	//单选组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	util.event('lay-set-radio',{
		add:function() {
			$(this).prev().append('<div><input type="radio" name="radio" value="" title="" lay-filter="set-radio-select"><input class="layui-input" value="选项"><a href="javascript:;" lay-set-radio="del"><i class="layui-icon layui-icon-reduce-circle"></i></a></div>');
			var id = _el.find('[data-type="radio"].active').attr('id'),
				inputName = _el.find('[data-type="radio"].active').find('input').attr('name');
			_el.find('[data-type="radio"].active').find('.layui-input-block').append('<input type="radio" value="" name="radio_' + inputName + '" title="选项">');
			form.render('radio',id);
			form.render(null,'components-setting-form');
			PARAMS[id].option.push({
				"title":"选项",
				"checked":false
			})
			console.log(PARAMS)
		},
		del:function() {
			var id = _el.find('[data-type="radio"].active').attr('id'),
				index = $(this).parent().index(),
				isChecked = $(this).siblings('input').prop("checked");
			if(isChecked) {
				$(this).parent().prev().children('input').prop('checked',true);
				_el.find('[data-type="radio"].active').find('input').eq(index-1).prop('checked',true);
				PARAMS[id].option[index-1].checked = true;
				form.render('radio',id);
				form.render(null,'components-setting-form');
			}
			$(this).parent().remove();
			_el.find('[data-type="radio"].active').find('input').eq(index).remove();
			_el.find('[data-type="radio"].active').find('.layui-form-radio').eq(index).remove();
			PARAMS[id].option.splice(index,1);
		},
	})

	//单选组件，更改选项名称
	$(document).on('input propertychange','.settings-change-input[type="radio"] input.layui-input',function() {
		var id = _el.find('[data-type="radio"].active').attr('id'),
			val = $(this).val(),
			index = $(this).parent().index();
		_el.find('[data-type="radio"].active').find('input').eq(index).attr('title',val);
		form.render('radio',id);
		PARAMS[id].option[index].title = val;		
	})
	//单选组件，选项操作
	form.on('radio(set-radio-select)',function(data) {
		var id = _el.find('[data-type="radio"].active').attr('id'),
			index = $(data.elem).parent().index();
		_el.find('[data-type="radio"].active').find('input').eq(index).prop('checked',true).siblings('input').prop('checked',false);
		_el.find('[data-type="radio"].active').find('input').eq(index).attr('checked',true).siblings('input').attr('checked',false);
		form.render('radio',id);
		PARAMS[id].option.map(function(d,i) {
			if(i == index) {
				PARAMS[id].option[i].checked = true;
			}else{
				PARAMS[id].option[i].checked = false;
			}
		})
		console.log(PARAMS[id])
	})

	//多选组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	util.event('lay-set-checkbox',{
		add:function() {
			$(this).prev().append('<div><input type="checkbox" name="checkbox" value="" title="" lay-skin="primary" lay-filter="set-checkbox-select"><input class="layui-input" value="选项" lay-set-checkbox="edit"><a href="javascript:;" lay-set-checkbox="del"><i class="layui-icon layui-icon-reduce-circle"></i></a></div>');
			var id = _el.find('[data-type="checkbox"].active').attr('id'),
				inputName = _el.find('[data-type="checkbox"].active').find('input').attr('name');
			_el.find('[data-type="checkbox"].active').find('.layui-input-block').append('<input type="checkbox" value="" name="checkbox_' + inputName + '" title="选项" lay-skin="primary">');
			form.render('checkbox');
			PARAMS[id].option.push({
				"title":"选项",
				"checked":false
			})
			console.log(id,PARAMS)
		},
		del:function() {
			var id = _el.find('[data-type="checkbox"].active').attr('id'),
				index = $(this).parent().index();
			$(this).parent().remove();
			_el.find('[data-type="checkbox"].active').find('input').eq(index).remove();
			_el.find('[data-type="checkbox"].active').find('.layui-form-checkbox').eq(index).remove();
			PARAMS[id].option.splice(index,1);
		},
	})
	//多选组件，更改选项名称
	$(document).on('input propertychange','.settings-change-input[type="checkbox"] input.layui-input',function() {
		var id = _el.find('[data-type="checkbox"].active').attr('id'),
			val = $(this).val(),
			index = $(this).parent().index();
		_el.find('[data-type="checkbox"].active').find('input').eq(index).attr('title',val);
		form.render('checkbox');
		PARAMS[id].option[index].title = val;
	})
	//多选组件，选项操作
	form.on('checkbox(set-checkbox-select)',function(data) {
		var id = _el.find('[data-type="checkbox"].active').attr('id'),
			index = $(data.elem).parent().index();
		_el.find('[data-type="checkbox"].active').find('input').eq(index).prop('checked',data.elem.checked).attr('checked',data.elem.checked);
		form.render('checkbox');
		PARAMS[id].option[index].checked = data.elem.checked;
		console.log(PARAMS[id])
	})

	//select组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	util.event('lay-set-select',{
		add:function() {
			var lastval = $(this).prev().children('div:last-child').children('input').val();
			var count = Number(lastval)+1;
			$(this).prev().append('<div><input type="radio" name="radio" value="'+count+'" title="" lay-filter="set-select-option"><input class="layui-input" value="option"><a href="javascript:;" lay-set-select="del"><i class="layui-icon layui-icon-reduce-circle"></i></a></div>');
			var id = _el.find('[data-type="select"].active').attr('id'),
				inputName = _el.find('[data-type="select"].active').find('select').attr('name');
			_el.find('[data-type="select"].active').find('select').append('<option vlaue="'+count+'">option</option>');
			form.render('radio');
			PARAMS[id].option.push({
				"name":"option",
				"checked":false,
				"vlaue":count
			})
		},
		del:function() {
			var id = _el.find('[data-type="select"].active').attr('id'),
				index = $(this).parent().index(),
				isChecked = $(this).siblings('input').prop("checked");
			if(isChecked) {
				$(this).parent().prev().children('input').prop('checked',true);
				_el.find('[data-type="select"].active').find('select>option').eq(index-1).prop('selected',true);
				PARAMS[id].option[index-1].checked = true;
				form.render();
			}
			$(this).parent().remove();
			_el.find('[data-type="select"].active').find('select>option').eq(index).remove();
			PARAMS[id].option.splice(index,1);
		},
	})
	//select组件，更改选项名称
	$(document).on('input propertychange','.settings-change-input[type="select"] input.layui-input',function() {
		var id = _el.find('[data-type="select"].active').attr('id'),
			val = $(this).val(),
			index = $(this).parent().index();
		_el.find('[data-type="select"].active').find('select>option').eq(index).html(val);
		form.render('select');
		PARAMS[id].option[index].name = val;		
	})	
	//select组件，选项操作
	form.on('radio(set-select-option)',function(data) {
		var id = _el.find('[data-type="select"].active').attr('id'),
			index = $(data.elem).parent().index();
		_el.find('[data-type="select"].active').find('select>option').eq(index).prop('selected',true).siblings().prop('selected',false);
		form.render('select');
		PARAMS[id].option.map(function(d,i) {
			if(i == index) {
				PARAMS[id].option[i].checked = true;
			}else{
				PARAMS[id].option[i].checked = false;
			}
		})
		console.log(PARAMS[id])
	})

	//inputNumber组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	$(document).on('input propertychange','input[lay-settings="inputnumber"]',function() {
		var id = _el.find('[data-type="InputNumber"].active').attr('id'),
			name = $(this).attr('name'),
			val = $(this).val();
		if(name == 'value') {
			 _el.find('[data-type="InputNumber"].active').find('input.fc-input-number').val(val).attr('value',val);
		}
		PARAMS[id].config[name] = val;
	});

	//switch组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	form.on('switch(set-switch)',function(data){
		var id = _el.find('[data-type="switch"].active').attr('id');
		if(data.elem.checked) {
			// _el.find('[data-type="switch"].active').find('input[type="checkbox"]').prop('checked',true);
			_el.find('[data-type="switch"].active').find('input[type="checkbox"]').attr('checked',true).prop('checked',true);
		}else{
			// _el.find('[data-type="switch"].active').find('input[type="checkbox"]').prop('checked',false);
			_el.find('[data-type="switch"].active').find('input[type="checkbox"]').attr('checked',false).prop('checked',false);
		}
		PARAMS[id].checked = data.elem.checked;
		setTimeout(function(){
			form.render('checkbox');
		},100)
	})

	//line组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//设置上间距
	setLineMarginTop = function(val) {
		var id = _el.find('[data-type="line"].active').attr('id');
		_el.find('[data-type="line"].active').find('.layui-form-line').css({marginTop:val+'px'});
		PARAMS[id].marginTop = val;
	}
	//设置下间距
	setLineMarginBottom = function(val) {
		var id = _el.find('[data-type="line"].active').attr('id');
		_el.find('[data-type="line"].active').find('.layui-form-line').css({marginBottom:val+'px'});
		PARAMS[id].marginBottom = val;
	}
	//设置实线虚线
	form.on('radio(set-line-border)',function(data) {
		var id = _el.find('[data-type="line"].active').attr('id');
		_el.find('[data-type="line"].active').find('.layui-form-line').css({'border-style':data.value});
		PARAMS[id].borderStyle = data.value;
	})

	//progress组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//设置progress尺寸
	form.on('radio(set-progress-size)',function(data) {
		var id = _el.find('[data-type="progress"].active').attr('id');
		if(data.value == 'big') {
			_el.find('[data-type="progress"].active').find('.layui-progress').addClass('layui-progress-big');
		}else{
			_el.find('[data-type="progress"].active').find('.layui-progress').removeClass('layui-progress-big');
		}
		PARAMS[id].size = data.value;
		console.log(PARAMS)
	})
	//设置progress是否显示文本
	form.on('switch(setProgressTxt)',function(data) {
		var id = _el.find('[data-type="progress"].active').attr('id');
		if(data.elem.checked) {
			_el.find('[data-type="progress"].active').find('.layui-progress').attr('lay-showPercent','true');
			_el.find('[data-type="progress"].active').find('.layui-progress-text').show();
		}else{
			_el.find('[data-type="progress"].active').find('.layui-progress').removeAttr('lay-showPercent');
			_el.find('[data-type="progress"].active').find('.layui-progress-text').hide();
		}
		PARAMS[id].showPercent = data.elem.checked;
		console.log(PARAMS)
	})

	//title组件，选项操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//设置标题文字
	$(document).on('input propertychange','input[lay-settings="titleFont"]',function() {
		var id = _el.find('[data-type="title"].active').attr('id'),
			val = $(this).val();
		_el.find('.layui-form-item.active').find('.layui-form-title').html(val);
		PARAMS[id].title = val;
	});
	//设置标题字号
	setTitleFontSize = function(val) {
		var id = _el.find('[data-type="title"].active').attr('id');
		_el.find('[data-type="title"].active').find('.layui-form-title').css({fontSize:val+'px'});
		PARAMS[id].fontSize = val;
	}
	//设置标题是否粗体
	form.on('switch(setTitleFontWeight)',function(data) {
		var id = _el.find('[data-type="title"].active').attr('id');
		if(data.elem.checked) {
			_el.find('[data-type="title"].active').find('.layui-form-title').css({fontWeight:700});
		}else{
			_el.find('[data-type="title"].active').find('.layui-form-title').css({fontWeight:500});
		}
		PARAMS[id].fontWeight = data.elem.checked;
	})

	//rate组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//颜色编辑
	function changeRateTheme(type,id) {
		var newId;
		if(id) {
			newId = id;
		}else{
			newId = _el.find('[data-type="rate"].active').attr('id');
		}
		colorpicker.render({
			elem: '#setRateColor',
			color:PARAMS[newId].theme,
			size:'xs',
			done:function(color) {
				var id = _el.find('[data-type="rate"].active').attr('id');
				var rateId = _el.find('[data-type="rate"].active').find('.rate').attr('id');
			    rate.render({
		      		elem: '#'+rateId,
		      		theme:color,
		      		value:PARAMS[newId].value
			    });
			    PARAMS[id].theme = color;
			}
		});
	}

	//编辑rate初始值
	setRateValue = function(val) {
		var id = _el.find('[data-type="rate"].active').attr('id');
		var rateId = _el.find('[data-type="rate"].active').find('.rate').attr('id');
	    rate.render({
      		elem: '#'+rateId,
      		value:val,
      		theme:PARAMS[id].theme
	    });
	    PARAMS[id].value = val;
	}
	//设置rate只读
	form.on('switch(setRateReadonly)',function(data) {
		var id = _el.find('[data-type="rate"].active').attr('id');
		PARAMS[id].readonly = data.elem.checked;
	})

	//link组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	$(document).on('input propertychange','input[lay-settings="setLinkText"]',function() {
		var id = _el.find('[data-type="link"].active').attr('id'),
			val = $(this).val();
		_el.find('[data-type="link"].active').find('a.link').html(val);
		PARAMS[id].text = val;
	});
	$(document).on('input propertychange','input[lay-settings="setLinkHref"]',function() {
		var id = _el.find('[data-type="link"].active').attr('id'),
			val = $(this).val();
		PARAMS[id].href = val;
	});

	//级联选择器组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	form.on('select(cascader-source)',function(data){
		console.log('选择的数据源：',data.value);
		var id = _el.find('[data-type="cascader"].active').attr('id');
		PARAMS[id].data = data.value;
	})

	//slider组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//slider开启输入框
	form.on('switch(setSliderInput)',function(data) {
		var id = _el.find('[data-type="slider"].active').attr('id');
		var sliderId = _el.find('[data-type="slider"].active').find('.slider').attr('id');
	    slider.render({
      		elem: '#'+sliderId,
      		value:PARAMS[id].value,
      		input:data.elem.checked,
      		min:PARAMS[id].min,
      		max:PARAMS[id].max,
      		step:PARAMS[id].step,
      		theme:PARAMS[id].theme,      		
	    });
		PARAMS[id].input = data.elem.checked;
	})
	//编辑slider初始值
	setSliderValue = function(val) {
		var id = _el.find('[data-type="slider"].active').attr('id');
		var sliderId = _el.find('[data-type="slider"].active').find('.slider').attr('id');
	    slider.render({
      		elem: '#'+sliderId,
      		value:val,
      		input:PARAMS[id].input,
      		min:PARAMS[id].min,
      		max:PARAMS[id].max,
      		step:PARAMS[id].step,
      		theme:PARAMS[id].theme,
	    });
	    PARAMS[id].value = val;
	}
	//编辑slider最小值
	setSliderMin = function(val) {
		var id = _el.find('[data-type="slider"].active').attr('id');
		var sliderId = _el.find('[data-type="slider"].active').find('.slider').attr('id');
	    slider.render({
      		elem: '#'+sliderId,
      		value:PARAMS[id].value,
      		input:PARAMS[id].input,
      		min:val,
      		max:PARAMS[id].max,
      		step:PARAMS[id].step,
      		theme:PARAMS[id].theme,
	    });
	    PARAMS[id].min = val;
	}
	//编辑slider最大值
	setSliderMax = function(val) {
		var id = _el.find('[data-type="slider"].active').attr('id');
		var sliderId = _el.find('[data-type="slider"].active').find('.slider').attr('id');
	    slider.render({
      		elem: '#'+sliderId,
      		value:PARAMS[id].value,
      		input:PARAMS[id].input,
      		min:PARAMS[id].min,
      		max:val,
      		step:PARAMS[id].step,
      		theme:PARAMS[id].theme,
	    });
	    PARAMS[id].max = val;
	}
	//编辑slider步长
	setSliderStep = function(val) {
		var id = _el.find('[data-type="slider"].active').attr('id');
		var sliderId = _el.find('[data-type="slider"].active').find('.slider').attr('id');
	    slider.render({
      		elem: '#'+sliderId,
      		value:PARAMS[id].value,
      		input:PARAMS[id].input,
      		min:PARAMS[id].min,
      		max:PARAMS[id].max,
      		step:val,
      		theme:PARAMS[id].theme,
	    });
	    PARAMS[id].step = val;
	}

	//editor组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//修改 编辑器 高度
	setEditorHeight = function(val) {
		var id = _el.find('.layui-form-item.active').attr('id');
		var editorId = _el.find('.layui-form-item.active').find('textarea').attr('id');
		// _el.find('.layui-form-item.active').find('textarea').css({height:val+'px'});
    	layedit.build(editorId,{
			tool: [
				'strong' //加粗
				,'italic' //斜体
				,'underline' //下划线
				,'del' //删除线
				,'|' //分割线
				,'left' //左对齐
				,'center' //居中对齐
				,'right' //右对齐
				,'link' //超链接
				,'unlink' //清除链接
				,'image' //插入图片
			],
    		height: val
    	});
		PARAMS[id].height = val;		
	}

	//upload img 组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//设置图片上传数量
	form.on('radio(set-uploadimg-number)',function(data) {
		var id = _el.find('[data-type="uploadImg"].active').attr('id');
		_el.find('[data-type="uploadImg"].active').find('.layui-form-line').css({'border-style':data.value});
		if(data.value == 'single') {
			PARAMS[id].multiple = false;
		}else{
			PARAMS[id].multiple = true;
		}
	})

	//栅格布局 组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//设置栅格布局间隔
	setGridLayoutSpace = function(val) {
		var _elActive = _el.find('.active');
		var id;
		if(_elActive.is('.column')) {
			id = _elActive.parents('.layui-form-item').attr('id');
			_elActive.parent().removeClass().addClass('layui-row layui-col-space'+val+'');
		}
		if(_elActive.is('.layui-form-item')) {
			id = _elActive.attr('id');
			_elActive.find('.layui-row').removeClass().addClass('layui-row layui-col-space'+val+'');
		}
		PARAMS[id].space = val;
	}
	//设置栅格布局一行列个数
	setGridLayoutCol = function(val) {
		var _elActive = _el.find('.active');
		var id,col;
		if(_elActive.is('.column')) {
			id = _elActive.parents('.layui-form-item').attr('id');
			col = _elActive.parent().find('.column');
			col.map(function(i,d){
				if($(d).is('.active')) {
					$(d).removeClass().addClass('active column layui-col-md'+12/val+'');
				}else{
					$(d).removeClass().addClass('column layui-col-md'+12/val+'');
				}
			})
		}
		if(_elActive.is('.layui-form-item')) {
			id = _elActive.attr('id');
			col = _elActive.find('.column');
			col.removeClass().addClass('column layui-col-md'+12/val+'');
		}
		PARAMS[id].col = val;
	}
	//添加列
	util.event('lay-set-gridLayout',{
		add:function() {
			var _elActive = _el.find('.active');
			if(_elActive.is('.column')) {
				var _clone = _elActive.parents('.layui-form-item').children('.layui-row').children().first().clone();
				_clone.removeAttr('full').removeClass('active');
				_clone.find('.layout').children('.layui-form-item').remove();				
				_elActive.parents('.layui-form-item').children('.layui-row').append(_clone);
			}
			if(_elActive.is('.layui-form-item')) {
				var _clone = _elActive.children('.layui-row').children().first().clone();
				_clone.removeAttr('full');
				_clone.find('.layout').children('.layui-form-item').remove();				
				_elActive.children('.layui-row').append(_clone);
			}
			el_draggable();
		},
	})	

	//表单布局 组件操作 ▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢▢
	//设置表单列宽度
	setFormLayoutWidth = function(val) {
		var _elActive = _el.find('.active');
		var id;
		if(_elActive.is('.layui-inline')) {
			id = _elActive.parents('.layui-form-item').attr('id');
			_elActive.parent().children().css({width:val+'px'});
		}
		if(_elActive.is('.layui-form-item')) {
			id = _elActive.attr('id');
			_elActive.children('.layui-inline').css({width:val+'px'});
		}
		PARAMS[id].colWidth = val;
	}
	//添加列
	util.event('lay-set-formlayout',{
		add:function() {
			var _elActive = _el.find('.active');
			if(_elActive.is('.layui-inline')) {
				_elActive.parent().append(_elActive.clone());
				_elActive.parent().children().last().removeClass('active')
			}
			if(_elActive.is('.layui-form-item')) {
				var _clone = _elActive.children().first().clone();
				_clone.removeAttr('full');
				_clone.children('.layui-form-item').remove();					
				_elActive.append(_clone);
			}
			el_draggable();
		},
	})

	// 生成新组件
	function newComponent(item,L1,L2) {
		var str = '';
		switch (item.name){
			case 'text':
				str='<div class="layui-form-item" data-type="text"><label class="layui-form-label" '+L1+'>'+item.label+'</label><div class="layui-input-block" '+L2+'><input type="text" class="layui-input" placeholder="'+item.placeholder+'"></div></div>';
				break;
			case 'textarea':
				str+= '<div class="layui-form-item" data-type="textarea">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'>'+
					'		<textarea class="layui-textarea" placeholder="'+item.placeholder+'" style="'+(item.height>100?"height:"+item.height+"px;":"") + (item.width?"width:"+item.width+"%;":"")+'"></textarea>'+
					'	</div>'+
					'</div>';
				break;
			case 'radio':
				var radom = Math.ceil(Math.random() * 1000000);
				var radioList = '';
				item.option.map(function(d,i){
					radioList += '<input type="radio" name="radio_' + radom + '" title="'+d.title+'" '+(d.checked==true?"checked":"")+'>';
				})
				str+= '<div class="layui-form-item" data-type="radio">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'>'+radioList+'</div>'+
					'</div>';
				break;
			case 'checkbox':
				var radom = Math.ceil(Math.random() * 1000000);
				var checkboxList = '';
				item.option.map(function(d,i){
					checkboxList += '<input type="checkbox" lay-skin="primary" name="checkbox_' + radom + '" title="'+d.title+'" '+(d.checked==true?"checked":"")+'>';
				})
				str+= '<div class="layui-form-item" data-type="checkbox">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'>'+checkboxList+'</div>'+
					'</div>';
				break;
			case 'select':
				var checkboxList = '';
				item.option.map(function(d,i){
					checkboxList += '<option value="'+d.vlaue+'" '+(d.checked==true?"selected":"")+'>'+d.name+'</option>';
				})
				str+= '<div class="layui-form-item" data-type="select">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><select>'+checkboxList+'</select></div>'+
					'</div>';
				break;
			case 'InputNumber':
				var config = item.config;
				str+= '<div class="layui-form-item" data-type="InputNumber">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><input class="fc-input-number" '+(config.value?"value="+config.value:"")+' '+(config.max?"max="+config.max:"")+' '+(config.min?"min="+config.min:"")+' '+(config.step?"step="+config.step:"")+' /></div>'+
					'</div>';
				break;
			case 'switch':
				str+= '<div class="layui-form-item" data-type="switch">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><input type="checkbox" name="switch" lay-skin="switch" '+(item.checked==true?"checked":"")+'></div>'+
					'</div>';
				break;
			case 'line':
				str+= '<div class="layui-form-item" data-type="line"><div class="layui-form-line" style="'+(item.marginTop?"margin-top:"+item.marginTop+"px;":"")+' '+(item.marginBottom?"margin-bottom:"+item.marginBottom+"px;":"")+' '+(item.borderStyle?"border-style:"+item.borderStyle:"")+'"></div></div>';
				break;
			case 'progress':
				str+= '<div class="layui-form-item" data-type="progress">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><div class="v-c"><div class="layui-progress '+(item.size=="big"?"layui-progress-big":"")+'" '+(item.showPercent?"lay-showPercent=yes":"")+' style="width:100%;"><div class="layui-progress-bar layui-bg-blue" lay-percent="0%"></div></div></div></div>'+
					'</div>';
				break;
			case 'title':
				str+= '<div class="layui-form-item" data-type="title">'+
					'	<h3 class="layui-form-title" style="'+(item.fontSize?"font-size:"+item.fontSize+"px;":"")+' '+(item.fontWeight?"font-weight:700;":"")+'">'+item.title+'</h3>'+
					'</div>';
				break;
			case 'rate':
				str+= '<div class="layui-form-item" data-type="rate">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><div id="'+item.id+'"></div></div>'+
					'</div>';
				break;
			case 'link':
				str+= '<div class="layui-form-item" data-type="link">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><div class="layui-form-mid"><a href="'+item.href+'" target="_blank">'+item.text+'</a></div></div>'+
					'</div>';
				break;
			case 'cascader':
				str+= '<div class="layui-form-item" data-type="cascader">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><input type="text" id="'+item.id+'" class="layui-input cascader" readonly="readonly"></div>'+
					'</div>';
				break;
			case 'slider':
				str+= '<div class="layui-form-item" data-type="slider">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><div class="v-c"><div id="'+item.id+'" class="slider" style="width:100%;"></div></div></div>'+
					'</div>';
				break;
			case 'dateRange':
				str+= '<div class="layui-form-item" data-type="dateRange">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><input type="text" class="layui-input lay-date" placeholder="'+item.placeholder+'" id="'+item.id+'" '+(item.width?"style=width:"+item.width+"%;":"")+'></div>'+
					'</div>';
				break;
			case 'date':
				str+= '<div class="layui-form-item" data-type="date">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-inline" '+L2+'><input type="text" class="layui-input lay-date" placeholder="'+item.placeholder+'" id="'+item.id+'" '+(item.width?"style=width:"+item.width+"%;":"")+'></div>'+
					'</div>';
				break;
			case 'time':
				str+= '<div class="layui-form-item" data-type="time">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-inline" '+L2+'><input type="text" class="layui-input lay-time" placeholder="'+item.placeholder+'" id="'+item.id+'" '+(item.width?"style=width:"+item.width+"%;":"")+'></div>'+
					'</div>';
				break;
			case 'editor':
				str+= '<div class="layui-form-item" data-type="editor">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'><textarea id="'+item.id+'" class="editor" style="display: none;"></textarea></div>'+
					'</div>';
				break;
			case 'uploadImg':
				str+= '<div class="layui-form-item" data-type="uploadImg">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'>'+
					'		<div class="upload-img '+(item.multiple?"multiple":"single")+'" id="'+item.id+'" >'+
					'			<div class="action"><i class="layui-icon '+(item.multiple?"layui-icon-addition":"layui-icon-upload-drag")+'"></i>'+(item.multiple?"":"<p>上传</p>")+'</div>'+
					'		</div>'+
					'	</div>'+
					'</div>';
				break;
			case 'uploadFile':
				str+= '<div class="layui-form-item" data-type="uploadFile">'+
					'	<label class="layui-form-label" '+L1+'>'+item.label+'</label>'+
					'	<div class="layui-input-block" '+L2+'>'+
					'		<div class="v-c">'+
					'			<button type="button" class="layui-btn layui-btn-sm layui-btn-normal" id="'+item.id+'"><i class="layui-icon layui-icon-upload"></i>上传文件</button>'+
					'		</div>'+
					'	</div>'+
					'</div>';
				break;
		}
		return str;
	};

	/**
	 * 获取json，重新生成布局预览
	 */
	function createView() {
		var v = $('.view-wrap');
		var size = FORMCONFIG.config.size,
			labelposition = FORMCONFIG.config.labelPosition,
			formStyle = FORMCONFIG.config.style,
			labelWidth = FORMCONFIG.config.labelWidth;
		var c ='';

		var L1 = labelWidth!=80?"style=width:"+labelWidth+"px":"",
			L2 = labelWidth!=80?"style=margin-left:"+(Number(labelWidth)+30)+"px":"";
		
		FORMCONFIG.layout.map(function(item,index) {
			if(item.name == 'gridLayout') {
				var column='';
				for(i=0;i<item.children.length;i++){
					var m='';
					if(item.children[i].length > 0){
						item.children[i].map(function(x,y){
							//console.log(x.name)
							m+=newComponent(x,L1,L2);
						})
					}
					column+='<div class="layui-col-md'+(12/item.col)+'">'+m+'</div>';
				}
				c+= '<div class="layui-row layui-col-space'+item.space+'" data-type="gridLayout">'+column+'</div>';
			}else if(item.name == 'formLayout') {
				var column='';
				for(i=0;i<item.children.length;i++){
					var m='';
					if(item.children[i].length > 0){
						item.children[i].map(function(x,y){
							m+=newComponent(x,L1,L2);
						})
					}
					column+='<div class="layui-inline">'+m+'</div>';
				}
				c+= '<div class="layui-form-item" data-type="formLayout">'+column+'</div>';
			}else{
				c+=newComponent(item,L1,L2);
			}
		})
		var f = '<form class="layui-form form-designer'+(formStyle=="pane"?" layui-form-pane":"")+'" size="'+size+'" labelposition="'+labelposition+'">'+c+'</form>';
		v.html(f);

		//重渲染各个组件
		element.init();
		form.render();
		fcInputNumber.render();
		componentsMap();
	}

	//查找布局中需重渲染的组件
	function componentsMap() {
		FORMCONFIG.layout.map(function(item,index) {
			if(item.name == 'gridLayout') {
				for(i=0;i<item.children.length;i++){
					if(item.children[i].length > 0){
						item.children[i].map(function(x,y){
							componentsRender(x);
						})
					}
				}
			}else if(item.name == 'formLayout') {
				for(i=0;i<item.children.length;i++){
					if(item.children[i].length > 0){
						item.children[i].map(function(x,y){
							componentsRender(x);
						})
					}
				}
			}else{
				componentsRender(item);
			}
		})
	}

	//某些新组件需通过id进行渲染或设置
	function componentsRender(item) {
		if(item.name == 'rate') {
			rate.render({
				elem: '#'+item.id,
				value:item.value,
				readonly:item.readonly,
				theme:item.theme,
			})			
		}

	    if(item.name == 'cascader') {
	        cascader({
	            elem: "#"+item.id,
	            data: [
	                {
	                    value: 'a',
	                    label: '选项一',
	                    children: [
	                        {
	                            value: 'AA1',
	                            label: 'aa1',
	                        },
	                        {
	                            value: 'BB1',
	                            label: 'bb1'
	                        }
	                    ]
	                },{
	                	value:'b',
	                	label: '选项二',
	                }
	            ],
	            // url: "/aa",
	            // type: "post",
	            // triggerType: "change",
	            // showLastLevels: true,
	            // where: {
	            //     a: "aaa"
	            // },
	            // value: ["B", "BB2", "BBB4"],
	            // changeOnSelect: true,
	            // canParentSelect:true,
	            success: function (valData,labelData) {
	                console.log(valData,labelData);
	            }
	        });
	    }

	    if(item.name == 'slider') {
			slider.render({
				"elem": '#'+item.id,
				"theme":item.theme,
				"value":parseInt(item.value),
				"input":item.input,
				"min":parseInt(item.min),
				"max":parseInt(item.max),
				"step":parseInt(item.step),
			});
	    }

	    if(item.name == 'dateRange') {
			laydate.render({
				elem: '#'+item.id,
				type: 'date',
				range:true,
			});
	    }

	    if(item.name == 'date') {
			laydate.render({
				elem: '#'+item.id,
				type: 'date',
			});
	    }

	    if(item.name == 'time') {
			laydate.render({
				elem: '#'+item.id,
				type: 'time',
			});
	    }

	    if(item.name == 'editor') {
	    	layedit.build(item.id,{
				tool: [
					'strong' //加粗
					,'italic' //斜体
					,'underline' //下划线
					,'del' //删除线
					,'|' //分割线
					,'left' //左对齐
					,'center' //居中对齐
					,'right' //右对齐
					,'link' //超链接
					,'unlink' //清除链接
					,'image' //插入图片
				],
	    		height: item.height?item.height:180
	    	});
	    }

		if(item.name == 'uploadImg') {
			if(item.multiple) {
				//多图片上传
				upload.render({
					elem: '#'+item.id
					,url: '' //上传接口
					,multiple: true
					,before: function(obj){
						obj.preview(function(index, file, result){
							var list = '<div class="upload-img multiple" uploaded><img src="'+result+'" alt="" /><div class="action del"><i class="layui-icon layui-icon-delete"></i><p>删除</p></div></div>';
							$('#'+item.id).before(list);
							$('#'+item.id).siblings('.upload-img').find('.del').on('click',function(){
								$(this).parent().remove();
							})
						});
					}
					,done: function(res){
					}
				});					
			}else{
				//单图片上传
				upload.render({
					elem: '#'+item.id
					,url: '' //上传接口
					,before: function(obj){
						obj.preview(function(index, file, result){
							var _that = $('#'+item.id);
							_that.attr('uploaded','');
							if(_that.children('img').length > 0) {
								_that.children('img').attr('src', result);
							}else{
								_that.prepend('<img src="'+result+'" alt="" />')
							}
						});
					}
					,done:function(res){
					}
				})
			}
		}

		if(item.name == 'uploadFile') {
			upload.render({
				elem:'#'+item.id
				,url:''
				,auto:false
				,done:function(res){

				}
			})
		}

	    //栅格组件，需重新绑定拖拽事件
	    if(item.name == 'gridLayout' || item.name == 'formLayout') {
	    	el_draggable();
	    }		
	}
})
